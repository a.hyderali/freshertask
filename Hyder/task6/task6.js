var APiUrl = "https://erp.arco.sa:65/";
var SerachCustomerID = "CIN0000150";
var operation = "A";
var pageSize = 5;
var contractdata = [];
var ticketList = []
var contractdatalength=0;
window.onload = function () {
	debugger
	GetMyTicket();
}


$(document).ready(function () {
	$("#ticketList").html("");
	$("#ticketList").html(
		"<thead>" +
		"	<tr>" +
		"	<th data-key='EnquiryId' data-order='ascending' onclick='sortorder(this)'>Ticket Id</th>" +
		"	<th data-key='CreatedDatetime' data-order='ascending' onclick='sortorder(this)'>Ticket Date</th>" +
		"	<th data-key='CreatedBy' data-order='ascending' onclick='sortorder(this)'>Created By</th>" +
		"	<th data-key='ContractNumber' data-order='ascending' onclick='sortorder(this)'>Contract Number</th>" +
		"	<th data-key='LabourNumber' data-order='ascending' onclick='sortorder(this)'>Employee ID</th>" +
		"	<th data-key='LabourName' data-order='ascending' onclick='sortorder(this)'>Employee Name</th>" +
		"	<th data-key='StatusName' data-order='ascending' onclick='sortorder(this)'>Status</th>" +
		"	<th data-key='TicketTypeName' data-order='ascending' onclick='sortorder(this)'>Ticket Type</th>" +
		"	<th data-key='PriorityName' data-order='ascending' onclick='sortorder(this)'>Priority</th>" +
		"	<th data-key='Accuracy' data-order='ascending' onclick='sortorder(this)'>Accuracy</th>" +
		"	<th data-key='Subject' data-order='ascending' onclick='sortorder(this)'>Subject</th>" +
		"	</tr>" +
		"</thead>" +
		"<tbody>" +
		"</tbody>"
	);

	$("#btnShowPopup").click(function () {
		debugger;
		GetCustomerAndContract();
		GetTicketType();
		GetPriority();
		document.getElementById("form").reset();
		document.getElementById("myContract_error").innerHTML = ""
		document.getElementById("mySubCategory_error").innerHTML = ""
		document.getElementById("myCategory_error").innerHTML = ""
		document.getElementById("ticketsubject_error").innerHTML = ""
		document.getElementById("description_error").innerHTML = ""
		document.getElementById("myTickettype_error").innerHTML = ""
		document.getElementById("myAssigntogroup_error").innerHTML = ""
		$("#MyPopup").modal("show");
	});


	$("#forward").click(function () {
		debugger;
		var totalrecord = $(".line-content").length;
		var last = Math.ceil(totalrecord / pageSize)
		showPage(last)
	});

	$("#backward").click(function () {
		debugger;
		showPage(1)
	});




	$('#myContract').on('input', function () {
		debugger
		var input = $(this);
		var is_name = input.val();
		if (is_name == "") {
			document.getElementById("myContract_error").innerHTML = "* contract is required";
		} else {
			document.getElementById("myContract_error").innerHTML = "";
		}
	});
	$('#mySubCategory').on('input', function () {
		debugger
		var input = $(this);
		var is_name = input.val();
		if (is_name == "") {
			document.getElementById("mySubCategory_error").innerHTML = "* subcategory is required";
		} else {
			document.getElementById("mySubCategory_error").innerHTML = "";
		}
	});

	$('#myCategory').on('input', function () {
		debugger
		var input = $(this);
		var is_name = input.val();
		if (is_name == "") {
			document.getElementById("myCategory_error").innerHTML = "* category is required"
		} else {
			document.getElementById("myCategory_error").innerHTML = ""
		}
	});

	$('#ticketsubject').on('input', function () {
		debugger
		var input = $(this);
		var is_name = input.val();
		if (is_name == "") {
			document.getElementById("ticketsubject_error").innerHTML = "* ticket subject is required"
		} else {
			document.getElementById("ticketsubject_error").innerHTML = ""
		}
	});
	$('#description').on('input', function () {
		debugger
		var input = $(this);
		var is_name = input.val();
		if (is_name == "") {
			document.getElementById("description_error").innerHTML = "* description is required"
		} else {
			document.getElementById("description_error").innerHTML = ""
		}
	});
	$('#myTickettype').on('input', function () {
		debugger
		var input = $(this);
		var is_name = input.val();
		if (is_name == "") {
			document.getElementById("myTickettype_error").innerHTML = "* ticket type is required"
		} else {
			document.getElementById("myTickettype_error").innerHTML = ""
		}
	});
	$('#myAssigntogroup').on('input', function () {
		debugger
		var input = $(this);
		var is_name = input.val();
		if (is_name == "") {
			document.getElementById("myAssigntogroup_error").innerHTML = "* depatment is required"
		} else {
			document.getElementById("myAssigntogroup_error").innerHTML = ""
		}
	});

	$("#myContract").change(function () {
		debugger
		var ContractId = $("#myContract").val();
		if (ContractId != "" && ContractId != null) {
			$.ajax({
				type: "GET",
				url: APiUrl + "/api/GetTicketIndContractAllEmployee?CustomerId=" + SerachCustomerID + "&ContractId=" + ContractId,
				success: successFunc,
				error: errorFunc
			});

			function successFunc(data) {
				debugger
				SetLabour(data);
			}

			function errorFunc(data) {
				alert(data.Data);
			}
		}
	});
	$("#myTickettype").change(function () {
		debugger
		var TicketTypeId = $("#myTickettype").val();
		if (TicketTypeId != "" && TicketTypeId != null) {
			$.ajax({
				type: "GET",
				url: APiUrl + "/api/GetTicketAssignedToGroupByTicketTypeId?TicketTypeID=" + TicketTypeId,
				success: successFunc,
				error: errorFunc
			});

			function successFunc(data) {
				debugger
				SetAssigntogroup(data);
			}

			function errorFunc(data) {
				alert(data.Data);
			}
		}
	});
	$("#myAssigntogroup").change(function () {
		debugger
		var AssignGroupId = $("#myAssigntogroup").val();
		if (AssignGroupId != "" && AssignGroupId != null) {
			GetAssignToByGroup(AssignGroupId);
			GetTicketGroupByDepatmentId(AssignGroupId);
		}
	});

	$("#myCategory").change(function () {
		debugger
		var CategoryId = $("#myCategory").val();
		if (CategoryId != "" && CategoryId != null) {
			$.ajax({
				type: "GET",
				url: APiUrl + "/api/SubGroupByGroup?id=" + CategoryId,
				success: successFunc,
				error: errorFunc
			});

			function successFunc(data) {
				debugger
				SetSubCategory(data);
			}

			function errorFunc(data) {
				alert(data.Data);
			}
		}
	});

	$("#pagin li a").click(function () {
		debugger
		$("#pagin li a").removeClass("current");
		$(this).addClass("current");
		showPage(parseInt($(this).text()))
	});
});


	function autocomplete(value) { 

		debugger
		document.getElementById('contractlist').innerHTML = ''; 
	
	for (var i = 0; i<contractdatalength; i++) { 
			var node = document.createElement("option"); 
			var val = document.createTextNode(contractdata[i].ContractNumber); 
			node.appendChild(val); 
			document.getElementById("contractlist").appendChild(node); 
			} 
	} 

function SetCustomer(params) {
	debugger;
	$("#myCustomer").html("");
	$('#myCustomer').append(
		$('<option></option>').val('').html('--select--')
	);
	$.each(params, function (val, text) {
		$('#myCustomer').append(
			$('<option></option>').val(val).html(text)
		);
	});
}

function SetContract(params) {
	debugger;
	$("#myContract").html("");
	$('#myContract').append(
		$('<option></option>').val('').html('--select--')
	);
	$.each(params, function (val, text) {
		$('#myContract').append(
			$('<option></option>').val(text.ContractNumber).html(text.ContractNumber)
		);
	});
}


function SetLabour(params) {
	debugger;
	$("#myLabour").html("");
	$('#myLabour').append(
		$('<option></option>').val('').html('--select--')
	);
	$.each(params, function (val, text) {
		$('#myLabour').append(
			$('<option></option>').val(text.EmployeeId).html(text.EmployeeId)
		);
	});
}

function SetTicketType(params) {
	debugger;
	$("#myTickettype").html("");
	$('#myTickettype').append(
		$('<option></option>').val('').html('--select--')
	);
	$.each(params, function (val, text) {
		$('#myTickettype').append(
			$('<option></option>').val(text.ID).html(text.Name)
		);
	});
}

function SetAssigntogroup(params) {
	debugger;
	$("#myAssigntogroup").html("");
	$('#myAssigntogroup').append(
		$('<option></option>').val('').html('--select--')
	);
	$.each(params, function (val, text) {
		$('#myAssigntogroup').append(
			$('<option></option>').val(text.ID).html(text.Name)
		);
	});
}

function SetAssignedTo(params) {
	debugger;
	$("#myAssignto").html("");
	$('#myAssignto').append(
		$('<option></option>').val('').html('--select--')
	);
	$.each(params, function (val, text) {
		$('#myAssignto').append(
			$('<option></option>').val(text.ID).html(text.Name)
		);
	});
}

function SetCategory(params) {
	debugger;
	$("#myCategory").html("");
	$('#myCategory').append(
		$('<option></option>').val('').html('--select--')
	);
	$.each(params, function (val, text) {
		$('#myCategory').append(
			$('<option></option>').val(text.TicketGroupID).html(text.TicketGroupName)
		);
	});
}

function SetSubCategory(params) {
	debugger;
	$("#mySubCategory").html("");
	$('#mySubCategory').append(
		$('<option></option>').val('').html('--select--')
	);
	$.each(params, function (val, text) {
		$('#mySubCategory').append(
			$('<option></option>').val(text.Id).html(text.Description)
		);
	});
}

function SetPriority(params) {
	debugger;
	$("#myPriority").html("");
	$('#myPriority').append(
		$('<option></option>').val('').html('--select--')
	);
	$.each(params, function (val, text) {
		$('#myPriority').append(
			$('<option></option>').val(text.ID).html(text.Name)
		);
	});
}

function Add() {
	debugger
	var data = new FormData();
	var fileInput = document.getElementById('uploadfile');
	for (i = 0; i < fileInput.files.length; i++) {
		data.append(fileInput.files[i].name, fileInput.files[i]);
	}
	var params = JSON.stringify({
		cusnam: "",
		custid: SerachCustomerID,
		emil: "",
		descp: $('#description').val(),
		contno: "",
		Page: "",
		PageSize: "",
		Priority: $('#myPriority').val(),
		Group: $('#myCategory').val(),
		SubGroup: $('#mySubCategory').val(),
		Subject: $('#ticketsubject').val(),
		AssignedTo: $('#myAssignto').val(),
		TicketType: $('#myTickettype').val(),
		TicketAssignGroup: $('#myAssigntogroup').val(),
		ContractNumber: $('#myContract').val(),
		LabourNumber: $('#myLabour').val(),
		TicketChannel: 2,
		UserId: "cc.user",
		accuracy: "",
	});
	$.ajax({

		type: "POST",
		url: APiUrl + '/api/CreateTicketNew?UpdateTicketFields=' + params,
		traditional: true,
		data: data,
		contentType: false,
		processData: false,
		success: successFunc,
		error: errorFunc
	});

	function successFunc(data) {
		alert(data);
	}

	function errorFunc(data) {
		alert(data);
	}
	GetMyTicket();
}


function submit() {
	debugger
	var isValid = true;
	if ($("#myContract").val() == "" || $("#myContract").val() == null) {
		isValid = false
		document.getElementById("myContract_error").innerHTML = "* contract is required"
	} else {
		document.getElementById("myContract_error").innerHTML = ""
	}

	if ($("#mySubCategory").val() == "" || $("#mySubCategory").val() == null) {
		isValid = false
		document.getElementById("mySubCategory_error").innerHTML = "* subcategory is required"
	} else {
		document.getElementById("mySubCategory_error").innerHTML = ""
	}
	if ($("#myCategory").val() == "" || $("#myCategory").val() == null) {
		isValid = false
		document.getElementById("myCategory_error").innerHTML = "* category is required"
	} else {
		document.getElementById("myCategory_error").innerHTML = ""
	}
	if ($("#ticketsubject").val() == "" || $("#ticketsubject").val() == null) {
		isValid = false
		document.getElementById("ticketsubject_error").innerHTML = "* ticket subject is required"
	} else {
		document.getElementById("ticketsubject_error").innerHTML = ""
	}
	if ($("#description").val() == "" || $("#description").val() == null) {
		isValid = false
		document.getElementById("description_error").innerHTML = "* description is required"
	} else {
		document.getElementById("description_error").innerHTML = ""
	}
	if ($("#myTickettype").val() == "" || $("#myTickettype").val() == null) {
		isValid = false
		document.getElementById("myTickettype_error").innerHTML = "* ticket type is required"
	} else {
		document.getElementById("myTickettype_error").innerHTML = ""
	}
	if ($("#myAssigntogroup").val() == "" || $("#myAssigntogroup").val() == null) {
		isValid = false
		document.getElementById("myAssigntogroup_error").innerHTML = "* depatment is required"
	} else {
		document.getElementById("myAssigntogroup_error").innerHTML = ""
	}
	if (isValid) {
		if (operation == "A") {
			Add();
		} else {
			Edit();
		}
		$("#MyPopup").modal("hide");
	} else {
		alert("please fill out the fields")
	}
}



function sortorder(value) {
	debugger
	var dataKey = value.getAttribute('data-key');
	var dataOrder = value.getAttribute('data-order');

	if (dataOrder == 'ascending') {
		value.setAttribute('data-order', 'descending')
	} else {
		value.setAttribute('data-order', 'ascending')
	}

	var sortedList = ticketList.sort(function (x, y) {
		var firstKey = x[dataKey];
		var secKey = y[dataKey];
		var comparison = 0;
		if (firstKey > secKey) {
			if (dataOrder == 'ascending') {
				comparison = 1
			} else {
				comparison = -1
			}
		} else if (firstKey < secKey) {
			if (dataOrder == 'ascending') {
				comparison = -1
			}
		} else {
			comparison = 1
		}
		return comparison;
	})
	List(sortedList);
}



function List(data) {
	$("#ticketList tbody").html("");
	for (var i in data) {
		var item = data[i];
		$("#ticketList tbody").append("<tr  class='line-content'>" +
			"	<td>" + item.EnquiryId + "</td>" +
			"	<td>" + item.CreatedDatetime + "</td>" +
			"	<td>" + item.CreatedBy + "</td>" +
			"	<td>" + item.ContractNumber + "</td>" +
			"	<td>" + (item.LabourNumber == null ?"" : item.LabourNumber)+ "</td>" +
			"	<td>" + (item.LabourName == null?"" : item.LabourName) + "</td>" +
			"	<td>" + item.StatusName + "</td>" +
			"	<td>" + item.TicketTypeName + "</td>" +
			"	<td>" + item.PriorityName + "</td>" +
			"	<td>" + (item.Accuracy ==null ? "" : item.Accuracy) + "</td>" +
			"	<td>" + (item.Subject ==null? "" : item.Subject) + "</td>" +
			"</tr>");
	}
	showPage(1);
}

function GetMyTicket() {
	debugger;
	var CustomerTicketList = [];
	var rootapi = "";
	var filterstatus = "";
	if (filterstatus && filterstatus != -2) {
		rootapi = APiUrl + "/api/GetMyTicket?CustomerId=" + SerachCustomerID + "&status=" + filterstatus;
	} else {
		rootapi = APiUrl + "/api/GetMyTicket?CustomerId=" + SerachCustomerID;
	}
	$.ajax({
		type: "GEt",
		url: rootapi,
		success: successFunc,
		error: errorFunc
	});

	function successFunc(data) {
		debugger
		ticketList = data.Data;
		List(data.Data);

	}

	function errorFunc(data) {
		alert(data.Data);
	}

}

function GetCustomerAndContract() {
	$.ajax({
		type: "GET",
		url: APiUrl + "/api/GetTicketCustomerDetails?CustomerId=" + SerachCustomerID,
		success: successFunc,
		error: errorFunc
	});

	function successFunc(data) {
		debugger
		// SetCustomer(data.Result1);  
		contractdata = data.Result2
		contractdatalength = contractdata.length;
		autocomplete();
		// SetContract(data.Result2);
	}

	function errorFunc(data) {
		alert(data.Data);
	}
}

function GetTicketType() {
	$.ajax({
		type: "GET",
		url: APiUrl + "/api/TickettypeList",
		success: successFunc,
		error: errorFunc
	});

	function successFunc(data) {
		debugger
		var Transcation = data.Data.filter(function (d) {
			if (d.ID != "1") {
				return (d);
			}
		});
		SetTicketType(Transcation);
	}

	function errorFunc(data) {
		alert(data.Data);
	}
}

function GetAssignToByGroup(AssignGroupId) {
	$.ajax({
		type: "GET",
		url: APiUrl + "api/GetAssignToByGroup?AssignGroupId=" + AssignGroupId,
		success: successFunc,
		error: errorFunc
	});

	function successFunc(data) {
		debugger
		SetAssignedTo(data);
	}

	function errorFunc(data) {
		alert(data);
	}
}

function GetTicketGroupByDepatmentId(AssignGroupId) {
	$.ajax({
		type: "GET",
		url: APiUrl + "api/GetTicketGroupByDepatmentId?TicketAssignGroupId=" + AssignGroupId,
		success: successFunc,
		error: errorFunc
	});

	function successFunc(data) {
		debugger
		SetCategory(data);
	}

	function errorFunc(data) {
		alert(data);
	}
}

function GetPriority() {
	$.ajax({
		type: "GET",
		url: APiUrl + "api/PriorityList",
		success: successFunc,
		error: errorFunc
	});

	function successFunc(data) {
		debugger
		SetPriority(data.Data);
	}

	function errorFunc(data) {
		alert(data);
	}
}

function showPage(page) {
	debugger
	if (page) {
		$("#currentpage").val(page);
		$(".line-content").hide();
		$(".line-content").each(function (n) {
			if (n >= pageSize * (page - 1) && n < pageSize * page)
				$(this).show();
		});
	}
}

function forwardright() {
	debugger;
	$("#currentpage").val();
	showPage(parseInt($("#currentpage").val()) + 1)
}

function forwardleft() {
	$("#currentpage").val();
	showPage(parseInt($("#currentpage").val()) - 1)
}