$(function () {
	$("#btnShowPopup").click(function () {
		$(".cpwd").show();
		$(".pwd").show();
		document.getElementById("form").reset();
		$("#MyPopup").modal("show");
	});
});
$( document ).ready(function() {
	$('#emailid').on('input', function () {
		debugger
		var input = $(this);
		var is_name = input.val();
		if (is_name && !validateEmail(is_name)) {
			document.getElementById("emailid_error").innerHTML = "Invalid EmailId";
		} else {
			document.getElementById("emailid_error").innerHTML = "";
		}
	});

	$('#confirmpassword').on('input', function () {
		debugger
		var input = $(this);
		var is_name = input.val();
		if (is_name != $('#password').val()) {
			document.getElementById("confirmpassword_error").innerHTML = "Invalid Password";
		} else {
			document.getElementById("confirmpassword_error").innerHTML = "";
		}
	});

	$('#confirmpassword').on('input', function () {
		debugger
		var input = $(this);
		var is_name = input.val();
		if (is_name != $('#password').val()) {
			document.getElementById("confirmpassword_error").innerHTML = "Invalid Password";
		} else {
			document.getElementById("confirmpassword_error").innerHTML = "";
		}
	});

	$('#studentname').on('input', function () {
		debugger
		var input = $(this);
		var is_name = input.val();
		if (is_name == "") {
			document.getElementById("studentname_error").innerHTML = "* student name is required"
		} else {
			document.getElementById("studentname_error").innerHTML = "";
		}
	});

	$('#fathername').on('input', function () {
		debugger
		var input = $(this);
		var is_name = input.val();
		if (is_name == "" ) {
			document.getElementById("fathername_error").innerHTML = "* father name is required"
		} else {
			document.getElementById("fathername_error").innerHTML = "";
		}
	});

	$('#dateofbirth').on('input', function () {
		debugger
		var input = $(this);
		var is_name = input.val();
		if (is_name =="") {
			document.getElementById("dateofbirth_error").innerHTML = "* dateofbirth is required"
		} else {
			document.getElementById("dateofbirth_error").innerHTML = "";
		}
	});

	$('#department').on('input', function () {
		debugger
		var input = $(this);
		var is_name = input.val();
		if (is_name =="") {
			document.getElementById("department_error").innerHTML = "* department is required"
		} else {
			document.getElementById("department_error").innerHTML = ""
		}
	});

	$('#address').on('input', function () {
		debugger
		var input = $(this);
		var is_name = input.val();
		if (is_name =="") {
			document.getElementById("address_error").innerHTML = "* address is required"
	} else {
		document.getElementById("address_error").innerHTML = ""
	}
	});
	$('#age').on('input', function () {
		debugger
		var input = $(this);
		var is_name = input.val();
		if (is_name =="") {
			document.getElementById("age_error").innerHTML = "* age is required"
	} else {
		document.getElementById("age_error").innerHTML = ""
	}
	});
	$('#password').on('input', function () {
		debugger
		var input = $(this);
		var is_name = input.val();
		if (is_name =="") {
			document.getElementById("password_error").innerHTML = "* password is required"
		} else {
			document.getElementById("password_error").innerHTML = ""
		}
	});
});
var operation = "A";
var selected_index = -1;
var Studentdata = localStorage.getItem("Studentdata");
Studentdata = JSON.parse(Studentdata);
if (Studentdata == null)
	Studentdata = [];
window.onload = function () {
	debugger
	List();
	var main_form = document.getElementById('form');
	if (main_form) {
		main_form.addEventListener('submit', function (event) {
			event.preventDefault();
			submit();
		}, false);
	}
}

function Add() {
	var data = JSON.stringify({
		StudentName: $("#studentname").val(),
		FatherName: $("#fathername").val(),
		DateOfBirth: $("#dateofbirth").val(),
		Department: $("#department").val(),
		Address: $("#address").val(),
		Age: $("#age").val(),
		EmailId: $("#emailid").val()
	});
	Studentdata.push(data);
	localStorage.setItem("Studentdata", JSON.stringify(Studentdata));
	alert("The data was saved.");
	List();
	return true;
}

function Edit() {
	debugger
	Studentdata[selected_index] = JSON.stringify({
		StudentName: $("#studentname").val(),
		FatherName: $("#fathername").val(),
		DateOfBirth: $("#dateofbirth").val(),
		Department: $("#department").val(),
		Address: $("#address").val(),
		Age: $("#age").val(),
		EmailId: $("#emailid").val()
	});
	localStorage.setItem("Studentdata", JSON.stringify(Studentdata));
	alert("record updated.")
	operation = "A";
	List();
	return true;
}
function validateEmail(emailField){
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	if (reg.test(emailField) == false) 
	{
		return false;
	}
	return true;
}
function Delete() {
	Studentdata.splice(selected_index, 1);
	localStorage.setItem("Studentdata", JSON.stringify(Studentdata));
	alert("record deleted.");
}

function submit() {
	debugger
	var isValid = true;
	if ($("#studentname").val() == "") {
		isValid = false
		document.getElementById("studentname_error").innerHTML = "* student name is required"
	} else {
		document.getElementById("studentname_error").innerHTML = ""
	}
	if ($("#fathername").val() == "") {
		isValid = false
		document.getElementById("fathername_error").innerHTML = "* father name is required"
	} else {
		document.getElementById("fathername_error").innerHTML = ""
	}
	if ($("#dateofbirth").val() == "") {
		isValid = false
		document.getElementById("dateofbirth_error").innerHTML = "* dateofbirth is required"
	} else {
		document.getElementById("dateofbirth_error").innerHTML = ""
	}
	if ($("#department").val() == "") {
		isValid = false
		document.getElementById("department_error").innerHTML = "* department is required"
	} else {
		document.getElementById("department_error").innerHTML = ""
	}
	if ($("#address").val() == "") {
		isValid = false
		document.getElementById("address_error").innerHTML = "* address is required"
	} else {
		document.getElementById("address_error").innerHTML = ""
	}
	if ($("#age").val() == "") {
		isValid = false
		document.getElementById("age_error").innerHTML = "* age is required"
	} else {
		document.getElementById("age_error").innerHTML = ""
	}
	if ($("#emailid").val() == "") {
		isValid = false
		document.getElementById("emailid_error").innerHTML = "* emailid is required"
	} else {
		document.getElementById("emailid_error").innerHTML = ""
	}

	if (operation == "A") {
		if ($("#password").val() == "") {
			isValid = false
			document.getElementById("password_error").innerHTML = "* password is required"
		} else {
			document.getElementById("password_error").innerHTML = ""
		}
		if ($("#confirmpassword").val() == "") {
			isValid = false
			document.getElementById("confirmpassword_error").innerHTML = "* confirmpassword is required"
		} else {
			document.getElementById("confirmpassword_error").innerHTML = ""
		}
	}
	if (isValid) {
		if (operation == "A") {
			Add();
		} else{
			Edit();
		}
		$("#MyPopup").modal("hide");
	} else {
		alert("please fill out the fields")
	}
}

function EditClick(params) {
	debugger
	operation = "E";
	selected_index = params;
	var data = JSON.parse(Studentdata[selected_index]);
	$("#studentname").val(data.StudentName);
	$("#fathername").val(data.FatherName);
	$("#dateofbirth").val(data.DateOfBirth);
	$("#department").val(data.Department);
	$("#address").val(data.Address);
	$("#age").val(data.Age);
	$("#emailid").val(data.EmailId);
	$("#studentname").focus();
	$(".cpwd").hide();
	$(".pwd").hide();
	$("#MyPopup").modal("show");
}

function DeleteClick(params) {
	debugger
	selected_index = params;
	Delete();
	List();
}


function List() {
	$("#studentList").html("");
	$("#studentList").html(
		"<thead>" +
		"	<tr>" +
		"	<th></th>" +
		"	<th>ID</th>" +
		"	<th>Student Name</th>" +
		"	<th>Father Name</th>" +
		"	<th>DateOfBirth</th>" +
		"	<th>Address</th>" +
		"	<th>Department</th>" +
		"	<th>Age</th>" +
		"	<th>Email</th>" +
		"	</tr>" +
		"</thead>" +
		"<tbody>" +
		"</tbody>"
	);
	for (var i in Studentdata) {
		var data = JSON.parse(Studentdata[i]);
		$("#studentList tbody").append("<tr>" +
			"	<td><input type='button' onclick='EditClick(" + i + ")' value='Edit' class='btn btn-primary'>  <input type='button' onclick='DeleteClick(" + i + ")' value='Delete' class='btn btn-danger'></td>" +
			"	<td>" + i + "</td>" +
			"	<td>" + data.StudentName + "</td>" +
			"	<td>" + data.FatherName + "</td>" +
			"	<td>" + data.DateOfBirth + "</td>" +
			"	<td>" + data.Address + "</td>" +
			"	<td>" + data.Department + "</td>" +
			"	<td>" + data.Age + "</td>" +
			"	<td>" + data.EmailId + "</td>" +
			"</tr>");
	}
}